package com.gb.versusapi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.sql.SQLOutput;

@SpringBootTest
class VerSusApiApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testNameLength() {
        String name = "김미나123123";
        int nameLength = name.length();

        Assert.state(nameLength == 3, "이름은 3글자여야만 합니다.");
    }

}
