package com.gb.versusapi.service;

import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {
    public static CommonResult getCommonResult() {
        CommonResult result = new CommonResult();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());

        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();

        result.setData(data);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());

        return result;
    }

    public static <T> ListResult<T> getListResult(List<T> list) {
        ListResult<T> result = new ListResult<>();

        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setList(list);
        result.setTotalCount((long)list.size());

        return result;
    }

}
