package com.gb.versusapi.service;


import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Comment;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.comment.*;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;

    public Comment getCommentData(long id) {
        return commentRepository.findById(id).orElseThrow();
    }

    /**
     *
     * @param member 유저가
     * @param voteBoard 자기가 투표한 게시글에
     * @param request 댓글 입력한다. 자기가 투표한곳에만 등록이 된다. 아니면 안된다.
     */
    public void setComment(Member member, VoteBoard voteBoard, CommentCreateRequest request) {
        Optional<Comment> memberVoteBoard = commentRepository.findByMemberAndVoteBoardOrderById(member, voteBoard);

        if (memberVoteBoard.isEmpty()) {
            if (Objects.equals(member.getId(), voteBoard.getMember().getId())) {
                Comment addData = new Comment();

                addData.setMember(member);
                addData.setVoteBoard(voteBoard);
                addData.setContents(request.getContents());
                addData.setRecommendNum(0L);
                addData.setDateCommentRegister(LocalDateTime.now());

                commentRepository.save(addData);
            }
        }
    }

    /**
     *
     * @return 댓글 입력한 게시글 전부 가져오기
     */
    public ListResult<CommentItemPage> getComments(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<Comment> comments = commentRepository.findAll(pageRequest);

        List<CommentItemPage> result = new LinkedList<>();
        for (Comment comment : comments) {
            CommentItemPage addItem = new CommentItemPage();

            addItem.setId(comment.getId());
            addItem.setMemberId(comment.getMember().getId());
            addItem.setMemberName(comment.getMember().getNickName());
            addItem.setBoardTopicA(comment.getVoteBoard().getBoard().getTopicA());
            addItem.setBoardTopicB(comment.getVoteBoard().getBoard().getTopicB());
            addItem.setVoteBoardId(comment.getVoteBoard().getId());
            addItem.setContents(comment.getContents());
            addItem.setRecommendNum(comment.getRecommendNum());
            addItem.setDateCommentRegister(comment.getDateCommentRegister());
            result.add(addItem);
        }

        ListResult<CommentItemPage> response = new ListResult<>();
        response.setList(result);
        response.setTotalCount(comments.getTotalElements());
        response.setTotalPage(comments.getTotalPages());
        response.setCurrentPage(comments.getPageable().getPageNumber()+1);
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());

        return response;
    }

    /**
     *
     * @param member 유저가 쓴 댓글
     * @return 리스트로 가져오기
     */
    public List<CommentItems> getMemberComment(Member member) {
        List<Comment> originData = commentRepository.findAllByMemberOrderByIdDesc(member);
        List<CommentItems> result = new LinkedList<>();

        for (Comment comment : originData) {
            CommentItems addItem = new CommentItems();

            addItem.setContents(comment.getContents());
            addItem.setRecommendNum(comment.getRecommendNum());
            addItem.setDateCommentRegister(comment.getDateCommentRegister());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param member 유저가 댓글단 게시글
     * @return 리스트로 가져오기
     */
    public List<CommentItem> getMemberComments(Member member) {
        List<Comment> originData = commentRepository.findAllByMemberOrderByIdDesc(member);
        List<CommentItem> result = new LinkedList<>();

        for (Comment comment : originData) {
            CommentItem addItem = new CommentItem();

            addItem.setId(comment.getId());
            addItem.setMemberId(comment.getMember().getId());
            addItem.setMemberName(comment.getMember().getNickName());
            addItem.setVoteBoardId(comment.getVoteBoard().getId());
            addItem.setContents(comment.getContents());
            addItem.setRecommendNum(comment.getRecommendNum());
            addItem.setDateCommentRegister(comment.getDateCommentRegister());
            result.add(addItem);
        }
        return result;
    }

    public ListResult<CommentVoteBoardItem> getBoardVoteComments(Board board, int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 30);
        Page<Comment> comments = commentRepository.findAllByVoteBoard_BoardOrderByIdAsc(board, pageRequest);

        List<CommentVoteBoardItem> result = new LinkedList<>();
        for (Comment comment : comments) {
            CommentVoteBoardItem addItem = new CommentVoteBoardItem();
            addItem.setId(comment.getId());
            addItem.setMemberNickname(comment.getMember().getNickName());
            addItem.setVoteBoardIsA(comment.getVoteBoard().getIsA());
            addItem.setRecommendNum(comment.getRecommendNum());
            addItem.setDateCommentRegister(comment.getDateCommentRegister());

            result.add(addItem);
        }

        ListResult<CommentVoteBoardItem> response = new ListResult<>();
        response.setList(result);
        response.setTotalCount(comments.getTotalElements());
        response.setTotalPage(comments.getTotalPages());
        response.setCurrentPage(comments.getPageable().getPageNumber()+1);
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());

        return response;
    }

    /**
     * 작성한 댓글 조회하여 댓글과 게시글 리스트, 투표한 거 보여주기
     *
     * @param id 작성한 댓글의 id
     * @return CommentResponse 모델
     */
    public CommentResponse getCommentResponse (long id) {
        Comment comment = commentRepository.findById(id).orElseThrow();

        CommentResponse response = new CommentResponse();

        response.setMemberId(comment.getMember().getId());
        response.setVoteBoardId(comment.getVoteBoard().getId());
        response.setContents(comment.getContents());
        response.setRecommendNum(comment.getRecommendNum());
        response.setDateCommentRegister(comment.getDateCommentRegister());

        response.setTopicA(comment.getVoteBoard().getBoard().getTopicA());
        response.setTopicB(comment.getVoteBoard().getBoard().getTopicB());

        response.setIsA(comment.getVoteBoard().getIsA() ? "A투표" : "B투표");

        return response;
    }


//    /**
//     *
//     * @param id 선택한 댓글 삭제
//     */
//    public void delComment(long id) {
//        commentRepository.deleteById(id);
//    }
}
