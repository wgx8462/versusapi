package com.gb.versusapi.service;

import com.gb.versusapi.entity.Forbid;
import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.forbid.ForbidCreateRequest;
import com.gb.versusapi.model.forbid.ForbidItem;
import com.gb.versusapi.repository.ForbidRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ForbidService {
    private final ForbidRepository forbidRepository;

    /**
     *
     * @param request 금칙어 등록하는데 중복이면 등록이 안된다.
     */
    public void setForbid (ForbidCreateRequest request) {
        Optional<Forbid> forbidNameList = forbidRepository.findByForbidName(request.getForbidName());

        if (forbidNameList.isEmpty()) {
            Forbid addData = new Forbid();

            addData.setForbidName(request.getForbidName());
            addData.setDateForbid(LocalDateTime.now());

            forbidRepository.save(addData);
        }
    }

    /**
     * 금칙어 리스트 조회
     *
     * @return List<ForbidList>
     */
    public List<ForbidItem> getForbids () {
        List<Forbid> originForbid = forbidRepository.findAll();
        List<ForbidItem> result = new LinkedList<>();

        for (Forbid forbid : originForbid) {
            ForbidItem addList = new ForbidItem();

            addList.setId(forbid.getId());
            addList.setForbidName(forbid.getForbidName());
            addList.setDateForbid(forbid.getDateForbid());

            result.add(addList);
        }
        return result;
    }

    public ListResult<Forbid> getForbids2(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<Forbid> forbids = forbidRepository.findAll(pageRequest);
        List<Forbid> result = new LinkedList<>();

        for (Forbid forbid : forbids) {
            Forbid addItem = new Forbid();
            addItem.setId(forbid.getId());
            addItem.setDateForbid(forbid.getDateForbid());
            addItem.setForbidName(forbid.getForbidName());
            result.add(addItem);
        }
        ListResult<Forbid> response = new ListResult<>();
        response.setList(result);
        response.setTotalCount(forbids.getTotalElements());
        response.setCurrentPage(forbids.getTotalPages());
        response.setTotalPage(forbids.getTotalPages());
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());

        return response;
    }

    /**
     * 금칙어 삭제
     *
     * @param id 금칙어 id
     */
    public void delForbid (long id) {
        forbidRepository.deleteById(id);
    }
}

