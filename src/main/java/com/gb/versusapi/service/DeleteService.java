package com.gb.versusapi.service;

import com.gb.versusapi.entity.*;
import com.gb.versusapi.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteService {
    private final RecommendCommentRepository recommendCommentRepository;
    private final CommentRepository commentRepository;
    private final VoteBoardRepository voteBoardRepository;
    private final RecommendBoardRepository recommendBoardRepository;
    private final BoardRepository boardRepository;

    /**
     *
     * @param id 게시글을 삭제하면 하위 fk 전부 삭제한다.
     */
    public void delBoardAllClear(long id) {
        Board board = boardRepository.findById(id).orElseThrow();
        List<RecommendBoard> recommendBoards = recommendBoardRepository.findByBoard(board);
        List<VoteBoard> voteBoards = voteBoardRepository.findByBoard(board);

        // 노가다 삭제 for문 써서 했는데 나중에는 프로시저 사용해서 삭제한다.
        for (VoteBoard voteBoard : voteBoards) {
            List<Comment> comments1 = commentRepository.findByVoteBoard(voteBoard);

            for (Comment comment : comments1) {
                List<RecommendComment> recommendComments = recommendCommentRepository.findByComment(comment);

                for (RecommendComment recommendComment : recommendComments) {
                    recommendCommentRepository.deleteById(recommendComment.getId());
                }
            }
        }

        for (VoteBoard voteBoard : voteBoards) {
            List<Comment> comments1 = commentRepository.findByVoteBoard(voteBoard);

            for (Comment comment : comments1) {
                commentRepository.deleteById(comment.getId());
            }
        }

        for (VoteBoard voteBoard : voteBoards) {
            voteBoardRepository.deleteById(voteBoard.getId());
        }

        for (RecommendBoard recommendBoard : recommendBoards) {
            recommendBoardRepository.deleteById(recommendBoard.getId());
        }

        boardRepository.deleteById(id);
    }

    /**
     *
     * @param id 투표를 삭제하면 하위 fk전부 삭제한다.
     */
    public void delVoteBoardAllClear(long id) {
        VoteBoard voteBoard = voteBoardRepository.findById(id).orElseThrow();
        List<Comment> comments = commentRepository.findByVoteBoard(voteBoard);

        for (Comment comment : comments) {
            List<RecommendComment> recommendComments = recommendCommentRepository.findByComment(comment);

            for (RecommendComment recommendComment : recommendComments) {
                recommendCommentRepository.deleteById(recommendComment.getId());
            }
        }
        for (Comment comment : comments) {
            commentRepository.deleteById(comment.getId());
        }

        if (voteBoard.getIsA()) {
            voteBoard.getBoard().setContentANum(voteBoard.getBoard().getContentANum()-1);
            boardRepository.save(voteBoard.getBoard());
        } else {
            voteBoard.getBoard().setContentBNum(voteBoard.getBoard().getContentBNum()-1);
            boardRepository.save(voteBoard.getBoard());
        }
        voteBoardRepository.deleteById(id);
    }

    /**
     *
     * @param id 댓글을 삭제 하면 하위 fk전부 삭제한다.
     */
    public void delCommentAllClear(long id) {
        Comment comment = commentRepository.findById(id).orElseThrow();
        List<RecommendComment> recommendComments = recommendCommentRepository.findByComment(comment);

        for (RecommendComment recommendComment : recommendComments) {
            recommendCommentRepository.deleteById(recommendComment.getId());
        }

        commentRepository.deleteById(id);
    }

}