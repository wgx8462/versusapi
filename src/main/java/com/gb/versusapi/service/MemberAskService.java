package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.MemberAsk;
import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.memberAsk.MemberAskCreateRequest;
import com.gb.versusapi.model.memberAsk.MemberAskEditRequest;
import com.gb.versusapi.model.memberAsk.MemberAskItem;
import com.gb.versusapi.model.memberAsk.MemberAskResponse;
import com.gb.versusapi.repository.MemberAskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class MemberAskService {
    private final MemberAskRepository memberAskRepository;

    public MemberAsk getMemberAskData(long id) {
        return memberAskRepository.findById(id).orElseThrow();
    }

    /**
     *
     * 문의글 작성
     *
     *
     * @param member
     * @param request
     */
    public void setMemberAsk (Member member, MemberAskCreateRequest request){
        MemberAsk addData = new MemberAsk();
        addData.setMember(member);
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());
        addData.setDateUserAsk(LocalDateTime.now());

        memberAskRepository.save(addData);
    }

    /**
     * 문의글 수정
     *
     * @param id
     * @param request
     */
    public void putAskEdit (long id, MemberAskEditRequest request){
        MemberAsk originData = memberAskRepository.findById(id).orElseThrow();
        originData.setTitle(request.getTitle());
        originData.setContent(request.getContent());
        originData.setDateUserAsk(LocalDateTime.now());

        memberAskRepository.save(originData);
    }

    /**
     *
     * @return 문의글 전체 리스트 보기
     */
    public List<MemberAskItem> getMemberAsks() {
        List<MemberAsk> originData = memberAskRepository.findAll();
        List<MemberAskItem> result = new LinkedList<>();

        for (MemberAsk memberAsk : originData) {
            MemberAskItem addItem = new MemberAskItem();

            addItem.setId(memberAsk.getId());
            addItem.setMemberNickName(memberAsk.getMember().getNickName());
            addItem.setTitle(memberAsk.getTitle());
            addItem.setDateUserAsk(memberAsk.getDateUserAsk());
            result.add(addItem);
        }
        return result;
    }

    public ListResult<MemberAskItem> getMemberAsks2(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1 , 10);
        Page<MemberAsk> memberAsks = memberAskRepository.findAll(pageRequest);

        List<MemberAskItem> result = new LinkedList<>();
        for (MemberAsk memberAsk : memberAsks) {
            MemberAskItem addItem = new MemberAskItem();

            addItem.setId(memberAsk.getId());
            addItem.setMemberNickName(memberAsk.getMember().getNickName());
            addItem.setTitle(memberAsk.getTitle());
            addItem.setDateUserAsk(memberAsk.getDateUserAsk());
            result.add(addItem);
        }

        ListResult<MemberAskItem> response = new ListResult<>();
        response.setList(result);
        response.setTotalPage(memberAsks.getTotalPages());
        response.setCurrentPage(memberAsks.getPageable().getPageNumber());
        response.setTotalCount(memberAsks.getTotalElements());
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());
        return response;
    }

    /**
     *
     * @param member 유저가 쓴 문의글
     * @return 전체 리스트로 모아보기
     */
    public List<MemberAskItem> getMemberAskList(Member member) {
        List<MemberAsk> originData = memberAskRepository.findAllByMemberOrderByIdDesc(member);
        List<MemberAskItem> result = new LinkedList<>();

        for (MemberAsk memberAsk : originData) {
            MemberAskItem addItem = new MemberAskItem();

            addItem.setId(memberAsk.getId());
            addItem.setMemberNickName(memberAsk.getMember().getNickName());
            addItem.setTitle(memberAsk.getTitle());
            addItem.setDateUserAsk(memberAsk.getDateUserAsk());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param id 문의글
     * @return 상세보기
     */
    public MemberAskResponse getMemberAsk(long id) {
        MemberAsk originData = memberAskRepository.findById(id).orElseThrow();
        MemberAskResponse response = new MemberAskResponse();

        response.setId(originData.getId());
        response.setMemberNickName(originData.getMember().getNickName());
        response.setTitle(originData.getTitle());
        response.setContent(originData.getContent());
        response.setDateUserAsk(originData.getDateUserAsk());

        return response;
    }

    /**
     * 문의글 삭제
     * @param id
     */
    public void delAsk(long id){
        memberAskRepository.deleteById(id);
    }
}

