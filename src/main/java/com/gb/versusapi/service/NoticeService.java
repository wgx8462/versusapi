package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.Notice;
import com.gb.versusapi.model.notice.NoticeChangeRequest;
import com.gb.versusapi.model.notice.NoticeCreateRequest;
import com.gb.versusapi.model.notice.NoticeItem;
import com.gb.versusapi.model.notice.NoticeResponse;
import com.gb.versusapi.repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoticeService {
    private final NoticeRepository noticeRepository;

    /**
     *
     * @param member 관리자가
     * @param request 공지글 등록
     */
    public void setNotice(Member member, NoticeCreateRequest request) {
        Notice addData = new Notice();

        addData.setMember(member);
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());
        addData.setDateNotice(LocalDateTime.now());

        noticeRepository.save(addData);
    }

    /**
     *
     * @return 공지글 리스트로 전부가져오기
     *  관리자아이디 공지글제목, 등록날짜
     */
    public List<NoticeItem> getNotices() {
        List<Notice> originList = noticeRepository.findAll();
        List<NoticeItem> result = new LinkedList<>();

        for (Notice notice : originList) {
            NoticeItem addItem = new NoticeItem();
            addItem.setId(notice.getId());
            addItem.setMemberName(notice.getMember().getNickName());
            addItem.setTitle(notice.getTitle());
            addItem.setDateNotice(notice.getDateNotice());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param id 공지글 시퀸스
     * @return 공지글 상세보기 내용 전부 가져오기
     */
    public NoticeResponse getNotice(long id) {
        Notice originData = noticeRepository.findById(id).orElseThrow();
        NoticeResponse response = new NoticeResponse();

        response.setId(originData.getId());
        response.setMemberName(originData.getMember().getNickName());
        response.setTitle(originData.getTitle());
        response.setContent(originData.getContent());
        response.setDateNotice(originData.getDateNotice());
        return response;
    }

    /**
     *
     * @param id 관리자가
     * @param request 공지글 제목이랑 내용 수정가능
     */
    public void putNotice(long id, NoticeChangeRequest request) {
        Notice originData = noticeRepository.findById(id).orElseThrow();

        originData.setTitle(request.getTitle());
        originData.setContent(request.getContent());
        noticeRepository.save(originData);
    }

    /**
     *
     * @param id 선택한 공지글 삭제
     */
    public void delNotice(long id) {
        noticeRepository.deleteById(id);
    }
}
