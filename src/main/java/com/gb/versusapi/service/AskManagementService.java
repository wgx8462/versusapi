package com.gb.versusapi.service;

import com.gb.versusapi.entity.AskManagement;
import com.gb.versusapi.entity.MemberAsk;
import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.askManagement.AskManagementCreateRequest;
import com.gb.versusapi.model.askManagement.AskManagementItem;
import com.gb.versusapi.model.askManagement.AskManagementResponse;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.repository.AskManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AskManagementService {
    private final AskManagementRepository askManagementRepository;

    /**
     *
     * @param memberAsk 유저가 쓴 문의글
     * @param request 관리자가 답변해주는 내용
     */
    public void setAskManagement(MemberAsk memberAsk, AskManagementCreateRequest request) {
        AskManagement addData = new AskManagement();

        addData.setMemberAsk(memberAsk);
        addData.setContent(request.getContent());
        addData.setDateAskManagement(LocalDateTime.now());
        addData.setIsCompletion(true);

        askManagementRepository.save(addData);
    }

    /**
     *
     * @return 유저가 쓴 게시글과 답변완료 여부 리스트로 가져오기
     */
    public List<AskManagementItem> getAskManagements() {
        List<AskManagement> originData = askManagementRepository.findAll();
        List<AskManagementItem> result = new LinkedList<>();

        for (AskManagement askManagement : originData) {
            AskManagementItem addItem = new AskManagementItem();

            addItem.setId(askManagement.getId());
            addItem.setMemberAskTitle(askManagement.getMemberAsk().getTitle());
            addItem.setDateAskManagement(askManagement.getDateAskManagement());
            addItem.setIsCompletion(askManagement.getIsCompletion());

            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param pageNum 유저가 쓴 게시글과 답변 완료 여부 리스트로 가져오기
     * @return 페이징처리
     */
    public ListResult<AskManagementItem> getAskManagements2(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 15);
        Page<AskManagement> askManagements = askManagementRepository.findAllByOrderByIdDesc(pageRequest);

        List<AskManagementItem> result = new LinkedList<>();
        for (AskManagement askManagement : askManagements) {
            AskManagementItem addItem = new AskManagementItem();

            addItem.setId(askManagement.getId());
            addItem.setMemberAskTitle(askManagement.getMemberAsk().getTitle());
            addItem.setDateAskManagement(askManagement.getDateAskManagement());
            addItem.setIsCompletion(askManagement.getIsCompletion());

            result.add(addItem);
        }

        ListResult<AskManagementItem> response = new ListResult<>();
        response.setList(result);
        response.setTotalPage(askManagements.getTotalPages());
        response.setCurrentPage(askManagements.getPageable().getPageNumber());
        response.setTotalCount(askManagements.getTotalElements());
        response.setMsg(ResultCode.SUCCESS.getMsg());
        response.setCode(ResultCode.SUCCESS.getCode());

        return response;
    }

    /**
     *
     * @param id 문의글 답변한 내용
     * @return 상세보기
     */
    public AskManagementResponse getAskManagement(long id) {
        AskManagement originData = askManagementRepository.findById(id).orElseThrow();
        AskManagementResponse response = new AskManagementResponse();

        response.setId(originData.getId());
        response.setMemberAskTitle(originData.getMemberAsk().getTitle());
        response.setMemberAskContent(originData.getMemberAsk().getContent());
        response.setContent(originData.getContent());
        response.setDateAskManagement(originData.getDateAskManagement());
        response.setIsCompletion(originData.getIsCompletion());

        return response;
    }

    public void delAskManagement(long id) {
        askManagementRepository.deleteById(id);
    }
}
