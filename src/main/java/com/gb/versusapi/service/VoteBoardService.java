package com.gb.versusapi.service;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.model.voteBoard.VoteBoardItem;
import com.gb.versusapi.repository.BoardRepository;
import com.gb.versusapi.repository.MemberRepository;
import com.gb.versusapi.repository.VoteBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class VoteBoardService {
    private final BoardRepository boardRepository;
    private final VoteBoardRepository voteBoardRepository;

    public VoteBoard getVoteBoardData(long id) {
        return voteBoardRepository.findById(id).orElseThrow();
    }

    public void setMembers(long id) {
        Optional<VoteBoard> voteBoard = voteBoardRepository.findVoteBoardByBoard_Member_Id(id);

        List<VoteBoard> sadasd = voteBoardRepository.findAll();

        VoteBoard addData = new VoteBoard();

        addData.setIsA(true);
        voteBoardRepository.save(addData);
    }

    /**
     *
     * @param member 유저가
     * @param board 게시글에 투표를 하는데
     *            등록이 안되어 있으면 A투표를 등록한다.
     *            등록이 되어있으면 A투표를 했는지 확인하고 A투표 했으면 삭제를한다. 만약 B투표가 등록되어 있으면 아무것도 하지 않는다.
     */
    public void setVoteBoardA(Member member, Board board) {
        Optional<VoteBoard> memberBoard = voteBoardRepository.findByMemberAndBoard(member, board);

        if (memberBoard.isEmpty()) {
            VoteBoard addData = new VoteBoard();
            addData.setMember(member);
            addData.setBoard(board);
            addData.setIsA(true);
            addData.setDateVote(LocalDateTime.now());

            board.setContentANum(board.getContentANum()+1);
            boardRepository.save(board);

            voteBoardRepository.save(addData);
//        } else if (memberBoard.get().getIsA()) {
//            board.setContentANum(board.getContentANum()-1);
//            boardRepository.save(board);
//
//            voteBoardRepository.delete(memberBoard.get()); // Optional 로 가져오면 있을수도 있고 없을수도 있다. .get()하면 그 모양 엔티티 가져온다.
        }
    }

    /**
     *
     * @param member 유저가
     * @param board 게시글에 투표를 하는데
     *            등록이 안되어 있으면 B투표를 등록한다.
     *            등록이 되어있으면 B투표를 했는지 확인하고 B투표 했으면 삭제를한다. 만약 A투표가 되어있으면 아무것도 하지 않는다.
     */
    public void setVoteBoardB(Member member, Board board) {
        Optional<VoteBoard> memberBoard = voteBoardRepository.findByMemberAndBoard(member, board);

        if (memberBoard.isEmpty()) {
            VoteBoard addData = new VoteBoard();
            addData.setMember(member);
            addData.setBoard(board);
            addData.setIsA(false);
            addData.setDateVote(LocalDateTime.now());

            board.setContentBNum(board.getContentBNum()+1);
            boardRepository.save(board);

            voteBoardRepository.save(addData);
//        } else if (!memberBoard.get().getIsA()) {
//            board.setContentBNum(board.getContentBNum()-1);
//            boardRepository.save(board);
//
//            voteBoardRepository.delete(memberBoard.get());
        }
    }

    /**
     *
     * @return 누가 어디에 투표했나 전체 리스트 그냥 데이터베이스에서 확인하기용
     */
    public List<VoteBoardItem> getVoteBoards() {
        List<VoteBoard> originData = voteBoardRepository.findAll();
        List<VoteBoardItem> result = new LinkedList<>();

        for (VoteBoard voteBoard : originData) {
            VoteBoardItem addItem = new VoteBoardItem();

            addItem.setId(voteBoard.getId());
            addItem.setMemberId(voteBoard.getMember().getId());
            addItem.setMemberName(voteBoard.getMember().getNickName());
            addItem.setBoardId(voteBoard.getBoard().getId());
            addItem.setBoardTopicA(voteBoard.getBoard().getTopicA());
            addItem.setBoardTopicB(voteBoard.getBoard().getTopicB());
            addItem.setIsAName(voteBoard.getIsA() ? "A투표" : "B투표");
            addItem.setDateVote(voteBoard.getDateVote());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param member 유저가 투표한 게시글
     * @return 유저가 투표한 게시글만 리스트로 가져오기
     */
    public List<VoteBoardItem> getMemberVoteBoards(Member member) {
        List<VoteBoard> originData = voteBoardRepository.findAllByMemberOrderByIdDesc(member);
        List<VoteBoardItem> result = new LinkedList<>();

        for (VoteBoard voteBoard : originData) {
            VoteBoardItem addItem = new VoteBoardItem();

            addItem.setId(voteBoard.getId());
            addItem.setMemberId(voteBoard.getMember().getId());
            addItem.setMemberName(voteBoard.getMember().getNickName());
            addItem.setBoardId(voteBoard.getBoard().getId());
            addItem.setBoardTopicA(voteBoard.getBoard().getTopicA());
            addItem.setBoardTopicB(voteBoard.getBoard().getTopicB());
            addItem.setIsAName(voteBoard.getIsA() ? "A투표" : "B투표");
            addItem.setDateVote(voteBoard.getDateVote());
            result.add(addItem);
        }
        return result;
    }

//    /**
//     *
//     * @param id 선택한 투표내용 삭제를 하는데 A투표가 되어있으면 삭제후 A카운트 다운한다. B투표가 되어있으면 삭제후 B카운트를 다운한다.
//     */
//    public void delVoteBoard(long id) {
//        VoteBoard voteBoard = voteBoardRepository.findById(id).orElseThrow();
//        Board board = boardRepository.findById(voteBoard.getBoard().getId()).orElseThrow();
//
//        if (voteBoard.getIsA()) {
//            board.setContentANum(board.getContentANum()-1);
//            boardRepository.save(board);
//        } else {
//            board.setContentBNum(board.getContentBNum()-1);
//            boardRepository.save(board);
//        }
//        voteBoardRepository.deleteById(id);
//    }
}
