package com.gb.versusapi.service;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Category;
import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.board.*;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    /**
     *
     * @param id 게시글 정보를 다른 서비스에 전달
     * @return 게시글 정보 전달
     */
    public Board getBoardData(long id) {
        return boardRepository.findById(id).orElseThrow();
    }

    /**
     * 새로운 게시글 등록
     *
     * @param member 게시글을 등록하는 유저 아이디
     * @param request 유저가 등록한 게시글 내용
     */
    public void setBoard(Member member, BoardCreateRequest request) {
        Board addData = new Board();

        addData.setMember(member);
        addData.setCategory(request.getCategory());
        addData.setPostType(request.getPostType());
        addData.setTopicA(request.getTopicA());
        addData.setContentA(request.getContentA());
        addData.setContentANum(0L);
        addData.setTopicB(request.getTopicB());
        addData.setContentB(request.getContentB());
        addData.setContentBNum(0L);
        addData.setDateBoard(LocalDateTime.now());
        addData.setViewCount(0L);
        addData.setLikeCount(0);
        addData.setIsOverTime(false);
        addData.setIsSecret(false);

        boardRepository.save(addData);
    }

    /**
     *
     * @return 게시글 리스트로 전부 가져오기 가장 마지막에 등록된 순서로
     * 게시글 등록한 아이디, 주제a, 주제b, 등록한 날짜, 게시글 조회수
     */
    public List<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1);
        List<BoardItem> result = new LinkedList<>();

        for (Board board : originList) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param pageNum 게시글 리스트 전부 가져오는데 최신술으로
     * @return 페이징처리
     */
    public ListResult<BoardItem> getBoards2(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<Board> boards = boardRepository.findAllByOrderByIdDesc(pageRequest);

        List<BoardItem> result = new LinkedList<>();
        for (Board board : boards.getContent()) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());

            result.add(addItem);
        }

        ListResult<BoardItem> response = new ListResult<>();
        response.setList(result);
        response.setTotalCount(boards.getTotalElements());
        response.setTotalPage(boards.getTotalPages());
        response.setCurrentPage(boards.getPageable().getPageNumber()+1);
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());


        return response;
    }

    /**
     *
     * @return 게시글 가져오는데 추천수가 10이상인 탑10만 가져온다.
     */
    public List<BoardItem> getLikeBoards() {
        List<Board> originData = boardRepository.findTop10ByLikeCountGreaterThanEqualOrderByLikeCountDesc(10);
        List<BoardItem> result = new LinkedList<>();

        for (Board board : originData) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());

            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param category 카테고리 선택하면 그 게시글만
     * @return 리스트로 보여준다.
     */
    public List<BoardItem> getCateBoards(Category category) {
        List<Board> originList = boardRepository.findAllByCategoryOrderByIdDesc(category);
        List<BoardItem> result = new LinkedList<>();

        for (Board board : originList) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param member 유저가 쓴 게시글
     * @return 유저가 쓴 게시글만 리스트로 가져오기
     */
    public List<BoardItem> getMemberBoards(Member member) {
        List<Board> originList = boardRepository.findAllByMemberOrderByIdDesc(member);

        List<BoardItem> result = new LinkedList<>();
        for (Board board : originList) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param member 유저가 쓴 게시글만 가져오는데
     * @param pageNum 페이징 처리해서
     * @return 가져오기
     */
    public ListResult<BoardItem> getMemberBoards2(Member member, int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<Board> boards = boardRepository.findAllByMemberAndIdGreaterThanEqualOrderByIdDesc(member, pageRequest, 1);

        List<BoardItem> result = new LinkedList<>();
        for (Board board : boards) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());
            result.add(addItem);
        }
        ListResult<BoardItem> response = new ListResult<>();
        response.setList(result);
        response.setTotalCount(boards.getTotalElements());
        response.setTotalPage(boards.getTotalPages());
        response.setCurrentPage(boards.getPageable().getPageNumber()+1);
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());

        return response;
    }

//    public List<BoardMemberSizeItem> getMemberBoardSize(Member member) {
//        List<Board> checkList = boardRepository.findAllByMemberOrderByIdDesc(member);
//        List<BoardMemberSizeItem> result = new LinkedList<>();
//
//        BoardMemberSizeItem addItem = new BoardMemberSizeItem();
//        addItem.setBooksSize(checkList.size());
//        result.add(addItem);
//
//        return result;
//    }

    /**
     *
     * @param id 게시글 시퀸스 번호
     * @return 게시글 하나에 대한 세부내용 전부 가져오기
     */
    public BoardResponse getBoard(long id) {
        Board originData = boardRepository.findById(id).orElseThrow();
        BoardResponse response = new BoardResponse();

        originData.setViewCount(originData.getViewCount() + 1);
        boardRepository.save(originData);

        response.setId(originData.getId());
        response.setMemberImgUrl(originData.getMember().getImgUrl());
        response.setMemberNickName(originData.getMember().getNickName());
        response.setCategoryName(originData.getCategory().getName());
        response.setPostTypeName(originData.getPostType().getName());
        response.setTopicA(originData.getTopicA());
        response.setContentA(originData.getContentA());
        response.setContentANum(originData.getContentANum());
        response.setTopicB(originData.getTopicB());
        response.setContentB(originData.getContentB());
        response.setContentBNum(originData.getContentBNum());
        response.setDateBoard(originData.getDateBoard());
        response.setViewCount(originData.getViewCount());
        response.setLikeCount(originData.getLikeCount());
        response.setIsOverTimeName(originData.getIsOverTime() ? "투표 끝남" : "투표 진행중");

        return response;
    }

    /**
     *
     * @param topicA 토픽A 내용 검색기능
     * @return 검색내용이랑 같은 게시글 전부 리스트로 가져온다.
     */
    public List<BoardItem> getTopicSearch(String topicA) {
        List<Board> originData = boardRepository.findAllByTopicAContaining(topicA);
        List<BoardItem> result = new LinkedList<>();

        for (Board board : originData) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());
            result.add(addItem);
        }
        return result;
    }

    public ListResult<BoardItem> getTopicABSearch(String topicA, String topicB, int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1 , 10);
        Page<Board> boards = boardRepository.findAllByTopicAContainingOrTopicBContaining(topicA, topicB, pageRequest);
        List<BoardItem> result = new LinkedList<>();

        for (Board board : boards) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberId(board.getMember().getId());
            addItem.setMemberImgUrl(board.getMember().getImgUrl());
            addItem.setMemberNickName(board.getMember().getNickName());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setDateBoard(board.getDateBoard());
            addItem.setViewCount(board.getViewCount());
            addItem.setLikeCount(board.getLikeCount());
            result.add(addItem);
        }
        ListResult<BoardItem> response = new ListResult<>();
        response.setList(result);
        response.setTotalPage(boards.getTotalPages());
        response.setCurrentPage(boards.getPageable().getPageNumber());
        response.setTotalCount(boards.getTotalElements());
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());
        return response;
    }

    /**
     * 잘못된 카테고리 변경
     *
     * @param id 잘못된 카테고리 게시글의 아이디
     * @param request 카테고리 변경 모델
     */
    public void putCategory (long id, BoardCategoryChangeRequest request) {
        Board board = boardRepository.findById(id).orElseThrow();

        board.setCategory(request.getCategory());
    }


//    /**
//     *
//     * @param id 선택한 게시글 삭제하기
//     */
//    public void delBoard(long id) {
//        boardRepository.deleteById(id);
//    }

}
