package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Gender;
import com.gb.versusapi.enums.Grade;
import com.gb.versusapi.enums.MemberRating;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.member.*;
import com.gb.versusapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;



@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 다른 엔티티에 회원 정보를 전달한다.
     *
     * @param id 회원의 시퀸스 id
     * @return
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    /**
     * 코드 리팩토링
     *
     * @param username 아이디 중복확인
     * @return 확인된 아이디 true 신규아이디 / false 중복아이디
     */
    public MemberDupCheckResponse getMemberIdDupCheck(String username) {
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUsername(username));

        return response;
    }

    /**
     * 회원을 가입시킨다.
     *
     * @param request 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    public void setMember(MemberCreateRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member member = new Member();
        member.setMemberRating(MemberRating.MEMBER);
        member.setGrade(Grade.BRONZE);
        member.setUsername(request.getUsername());
        member.setPassword(request.getPassword());
        member.setNickName(request.getNickName());
        member.setName(request.getName());
        member.setDateBirth(request.getDateBirth());
        member.setPhoneNumber(request.getPhoneNumber());
        member.setGender(request.getGender());
        member.setEmail(request.getEmail());
        member.setDateUser(LocalDate.now());

        memberRepository.save(member);
    }

    /**
     *
     * @return 회원 정보 리스트로 보기
     */
    public List<MemberItem> getMembers() {
        List<Member> originData = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originData) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setMemberRatingName(member.getMemberRating().getName());
            addItem.setGradeName(member.getGrade().getName());
            addItem.setUsername(member.getUsername());
            addItem.setNickName(member.getNickName());
            addItem.setName(member.getName());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param pageNum 회원 정보 페이징
     * @return 리스트로 보기
     */
    public ListResult<Member> getMembers2(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 10);
        Page<Member> members = memberRepository.findAll(pageRequest);

        ListResult<Member> result = new ListResult<>();
        result.setList(members.getContent());
        result.setTotalCount(members.getTotalElements());
        result.setTotalPage(members.getTotalPages());
        result.setCurrentPage(members.getPageable().getPageNumber()+1);
        result.setCode(0);
        result.setMsg("성공하였습니다.");

        return result;
    }

    /**
     *
     * @param id 선택한 회원 정보
     * @return 상세보기
     */
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();

        response.setId(originData.getId());
        response.setMemberRatingName(originData.getMemberRating().getName());
        response.setGradeName(originData.getGrade().getName());
        response.setUsername(originData.getUsername());
        response.setImgUrl(originData.getImgUrl());
        response.setNickName(originData.getNickName());
        response.setName(originData.getName());
        response.setDateBirth(originData.getDateBirth());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setGender(originData.getGender().getName());
        response.setEmail(originData.getEmail());

        return response;
    }

    /**
     *
     * @param name 유저이름 검색하기
     * @return 검색완료
     */
    public List<MemberNameSearchRequest> getSearch(String name) {
        List<Member> originData = memberRepository.findAllByNameContaining(name);
        List<MemberNameSearchRequest> result = new LinkedList<>();

        for (Member member : originData) {
            MemberNameSearchRequest addItem = new MemberNameSearchRequest();
            addItem.setUsername(member.getUsername());
            addItem.setNickName(member.getNickName());
            addItem.setName(member.getName());
            addItem.setGender(member.getGender().getName());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param username 유저아이디 검색하기
     * @return 검색완료
     */
    public MemberResponse getSearchUsername(String username) {
        Member originData = memberRepository.findByUsernameLike(username);
        MemberResponse response = new MemberResponse();

        response.setId(originData.getId());
        response.setMemberRatingName(originData.getMemberRating().getName());
        response.setGradeName(originData.getGrade().getName());
        response.setUsername(originData.getUsername());
        response.setImgUrl(originData.getImgUrl());
        response.setNickName(originData.getNickName());
        response.setName(originData.getName());
        response.setDateBirth(originData.getDateBirth());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setGender(originData.getGender().getName());
        response.setEmail(originData.getEmail());
        return response;
    }

    /**
     *
     * @return 멤버 테이블에 남자 여자 수 리스트로 가져오기
     */
    public List<MemberGenderStats> getGenders() {
        List<Member> originData = memberRepository.findAllByGender(Gender.MAN);
        List<Member> originData2 = memberRepository.findAllByGender(Gender.WOMAN);
        List<MemberGenderStats> result = new LinkedList<>();

        MemberGenderStats addItem = new MemberGenderStats();
        addItem.setManStats(originData.size());
        addItem.setWomanStats(originData2.size());
        result.add(addItem);

        return result;
    }


    /**
     * 닉네임 변경하기
     *
     * @param id 변경하고 싶은 id
     * @param request 닉네임 변경 틀
     */
    public void putNickName (long id, MemberNickNameChangeRequest request) {
        Member useNickName = memberRepository.findById(id).orElseThrow();

        useNickName.setNickName(request.getNickName());

        memberRepository.save(useNickName);
    }

    /**
     * 비밀번호 변경하기
     *
     * @throws Exception 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    public void putPassword (long id, MemberPasswordChangeRequest request) throws Exception {
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member usePassword = memberRepository.findById(id).orElseThrow();

        usePassword.setPassword(request.getPassword());

        memberRepository.save(usePassword);
    }


    /**
     * 회원의 상태를 변경한다.
     *
     * @param id 회원 시퀸스 id
     * @param request 바꿀 새로운 상태
     */
    public void putMemberRatingChange(long id, MemberRatingChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();

        originData.setMemberRating(request.getMemberRating());

        memberRepository.save(originData);
    }

    /**
     * 회원의 등급을 변경한다.
     *
     * @param id 회원 시퀸스 id
     * @param request 바꿀 새로운 등급
     */
    public void putMemberGradeChange(long id, MemberGradeChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();

        originData.setGrade(request.getGrade());

        memberRepository.save(originData);
    }

    /**
     * 신규 아이디인지 확인한다.
     *
     * @param username 아이디
     * @return true 신규아이디 / false 중복아이디
     */
    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount <= 0; //연산 순서랑 관련 있다.
    }



}
