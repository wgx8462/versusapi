package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Gender;
import com.gb.versusapi.enums.Grade;
import com.gb.versusapi.enums.MemberRating;
import com.gb.versusapi.lib.CommonFile;
import com.gb.versusapi.model.temp.NearDayStatisticsResponse;
import com.gb.versusapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TempService {
    private final MemberRepository memberRepository;

    /**
     *
     * @param multipartFile 데이터 베이스에 회원 정보 한번에 업로드
     * @throws IOException
     */
    public void setMemberByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 버퍼에서 넘어온 string 한줄을 임시로 담아 둘 변수가 필요하다.
        String line = "";

        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1씩 증가해서 기록 해 둘 변수가 필요하다.
        int index = 0;

        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 11) {

                    System.out.println(cols);
                    // 이제 db에 넣자.
                    Member addData = new Member();
                    addData.setMemberRating(MemberRating.valueOf(cols[0]));
                    addData.setGrade(Grade.valueOf(cols[1]));
                    addData.setUsername(cols[2]);
                    addData.setPassword(cols[3]);
                    addData.setNickName(cols[4]);
                    addData.setName(cols[5]);
                    addData.setDateBirth(LocalDate.parse(cols[6]));
                    addData.setPhoneNumber(cols[7]);
                    addData.setGender(Gender.valueOf(cols[8]));
                    addData.setEmail(cols[9]);
                    addData.setDateUser(LocalDate.parse(cols[10]));

                    memberRepository.save(addData);
                }
            }
            index++;
        }
        bufferedReader.close();
    }

    /**
     * @return 최근 7일간의 회원 가입자 수를 구한다.
     */

    public NearDayStatisticsResponse getNearDayStatistics() {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(6);

        NearDayStatisticsResponse response = new NearDayStatisticsResponse();

        List<String> labels = new LinkedList<>();
        List<Long> datasets = new LinkedList<>();
        for (int i = 0; i < 7; i++) {
            labels.add(startDay.plusDays(i).toString());
            datasets.add(memberRepository.countByDateUser(startDay.plusDays(i)));
        }
        response.setLabels(labels);
        response.setDatasets(datasets);

        return response;
    }
}
