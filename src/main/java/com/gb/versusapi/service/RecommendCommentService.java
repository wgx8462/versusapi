package com.gb.versusapi.service;

import com.gb.versusapi.entity.Comment;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendComment;
import com.gb.versusapi.model.recommendComment.RecommendCommentIsLikeItem;
import com.gb.versusapi.model.recommendComment.RecommendCommentItem;
import com.gb.versusapi.repository.CommentRepository;
import com.gb.versusapi.repository.RecommendCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RecommendCommentService {
    private final CommentRepository commentRepository;
    private final RecommendCommentRepository recommendCommentRepository;

    /**
     *
     * @param member 유저가 댓글에 좋아요
     * @param comment 한번 누르면 좋아요 한번더 누르면 취소
     */
    public void setRecommendComment(Member member, Comment comment) {
        Optional<RecommendComment> memberComment = recommendCommentRepository.findByMemberAndComment(member, comment);

        if (memberComment.isEmpty()) {
            RecommendComment addData = new RecommendComment();

            addData.setMember(member);
            addData.setComment(comment);
            addData.setIsLike(true);

            comment.setRecommendNum(comment.getRecommendNum()+1);
            commentRepository.save(comment);

            recommendCommentRepository.save(addData);
        } else {
            comment.setRecommendNum(comment.getRecommendNum()-1);
            commentRepository.save(comment);

            recommendCommentRepository.delete(memberComment.get());
        }
    }

    /**
     *
     * @param member 유저가 좋아요한 댓글
     * @return 좋아요한 댓글 게시글 전부 가져오기
     */
    public List<RecommendCommentIsLikeItem> getCommentIsLikes(Member member) {
        List<RecommendComment> originData = recommendCommentRepository.findAllByMemberOrderByComment_IdDesc(member);
        List<RecommendCommentIsLikeItem> result = new LinkedList<>();

        for (RecommendComment recommendComment : originData) {
            RecommendCommentIsLikeItem addItem = new RecommendCommentIsLikeItem();

            addItem.setId(recommendComment.getId());
            addItem.setMemberNickName(recommendComment.getComment().getVoteBoard().getBoard().getMember().getNickName());
            addItem.setBoardId(recommendComment.getComment().getVoteBoard().getBoard().getId());
            addItem.setBoardTopicA(recommendComment.getComment().getVoteBoard().getBoard().getTopicA());
            addItem.setBoardTopicB(recommendComment.getComment().getVoteBoard().getBoard().getTopicB());
            addItem.setBoardDateBoard(recommendComment.getComment().getVoteBoard().getBoard().getDateBoard());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @return 게시글 안에 댓글에 좋아요한 게시글 전부 가져오기 확인용
     */
    public List<RecommendCommentItem> getRecommendComments() {
        List<RecommendComment> originData = recommendCommentRepository.findAll();
        List<RecommendCommentItem> result = new LinkedList<>();

        for (RecommendComment recommendComment : originData) {
            RecommendCommentItem addItem = new RecommendCommentItem();

            addItem.setId(recommendComment.getId());
            addItem.setMemberName(recommendComment.getMember().getNickName());
            addItem.setCommentId(recommendComment.getComment().getId());
            addItem.setIsLike(recommendComment.getIsLike() ? "좋아요" : "싫어요");
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param id 선택한 댓글 좋아요 삭제
     */
    public void delRecommendComment(long id) {
        Optional<RecommendComment> recommendComment = recommendCommentRepository.findById(id);

        recommendComment.get().getComment().setRecommendNum(recommendComment.get().getComment().getRecommendNum()-1);
        commentRepository.save(recommendComment.get().getComment());

        recommendCommentRepository.deleteById(id);
    }

//    /**
//     *
//     * @param comment 댓글에 달린 좋아요 전부 삭제
//     */
//    public void delCommentLists(Comment comment) {
//        List<RecommendComment> commentList = recommendCommentRepository.findAllByComment(comment);
//
//        for (RecommendComment recommendComment : commentList) {
//            if (!commentList.isEmpty()) {
//                comment.setRecommendNum(recommendComment.getComment().getRecommendNum()-1);
//            }
//        }
//        recommendCommentRepository.deleteAll(commentList);
//    }
}
