package com.gb.versusapi.service;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendBoard;
import com.gb.versusapi.model.recommendBoard.RecommendBoardIsLikeItem;
import com.gb.versusapi.model.recommendBoard.RecommendBoardItem;
import com.gb.versusapi.repository.BoardRepository;
import com.gb.versusapi.repository.RecommendBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RecommendBoardService {
    private final BoardRepository boardRepository;
    private final RecommendBoardRepository recommendBoardRepository;

    /**
     *
     * @param member 유저가 클릭하면 좋아요 누르고 한번더 누르면 취소
     * @param board 클릭하면 카운트 올라가고 한번더 누르면 카운트가 내려간다.
     */
    public void setRecommendBoard(Member member, Board board) {
        Optional<RecommendBoard> memberBoard = recommendBoardRepository.findByMemberAndBoard(member, board);

        if (memberBoard.isEmpty()) {
            RecommendBoard addData = new RecommendBoard();
            addData.setMember(member);
            addData.setBoard(board);
            addData.setIsLike(true);
            addData.setDateRecommendBoard(LocalDateTime.now());

            board.setLikeCount(board.getLikeCount()+1);

            boardRepository.save(board);
            recommendBoardRepository.save(addData);
        } else {
            board.setLikeCount(board.getLikeCount()-1);

            boardRepository.save(board);
            recommendBoardRepository.delete(memberBoard.get());
        }
    }

    /**
     *
     * @param member 유저가
     * @return 좋아요한 게시글 전부 리스트로 가져오기
     */
    public List<RecommendBoardIsLikeItem> getRecommendBoardIsLikes(Member member) {
        List<RecommendBoard> originData = recommendBoardRepository.findAllByMemberOrderByBoard_IdDesc(member);
        List<RecommendBoardIsLikeItem> result = new LinkedList<>();

        for (RecommendBoard recommendBoard : originData) {
            RecommendBoardIsLikeItem addItem = new RecommendBoardIsLikeItem();

            addItem.setId(recommendBoard.getId());
            addItem.setMemberNickName(recommendBoard.getMember().getNickName());
            addItem.setDateRecommendBoard(recommendBoard.getDateRecommendBoard());
            addItem.setBoardMemberNickName(recommendBoard.getBoard().getMember().getNickName());
            addItem.setBoardId(recommendBoard.getBoard().getId());
            addItem.setBoardTopicA(recommendBoard.getBoard().getTopicA());
            addItem.setBoardTopicB(recommendBoard.getBoard().getTopicB());
            addItem.setBoardDateBoard(recommendBoard.getBoard().getDateBoard());
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @return 게시글 좋아요한 게시글 전부 가져오기
     */
    public List<RecommendBoardItem> getRecommendBoards() {
        List<RecommendBoard> originData = recommendBoardRepository.findAll();
        List<RecommendBoardItem> result = new LinkedList<>();

        for (RecommendBoard recommendBoard : originData) {
            RecommendBoardItem addItem = new RecommendBoardItem();

            addItem.setId(recommendBoard.getId());
            addItem.setMemberName(recommendBoard.getMember().getNickName());
            addItem.setBoardId(recommendBoard.getBoard().getId());
            addItem.setIsLike(recommendBoard.getIsLike() ? "좋아요" : "싫어요");
            result.add(addItem);
        }
        return result;
    }

    /**
     *
     * @param id 선택한 게시글 좋아요 삭제
     */
    public void delRecommendBoard(long id) {
        Optional<RecommendBoard> recommendBoard = recommendBoardRepository.findById(id);
        recommendBoard.get().getBoard().setLikeCount(recommendBoard.get().getBoard().getLikeCount()-1);

        boardRepository.save(recommendBoard.get().getBoard());

        recommendBoardRepository.deleteById(id);
    }

//    /**
//     *
//     * @param board 게시글 좋아요 전부 삭제
//     */
//    public void delBoardList(Board board) {
//        List<RecommendBoard> boardList = recommendBoardRepository.findAllByBoard(board);
//
//        for (RecommendBoard recommendBoard : boardList) {
//            if (!boardList.isEmpty()) {
//                board.setLikeCount(recommendBoard.getBoard().getLikeCount()-1);
//            }
//        }
//        recommendBoardRepository.deleteAll(boardList);
//    }
}
