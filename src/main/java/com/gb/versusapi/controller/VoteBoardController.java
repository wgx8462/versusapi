package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.voteBoard.VoteBoardItem;
import com.gb.versusapi.service.BoardService;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.ResponseService;
import com.gb.versusapi.service.VoteBoardService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/vote-board")
public class VoteBoardController {
    private final MemberService memberService;
    private final BoardService boardService;
    private final VoteBoardService voteBoardService;

    /**
     *
     * @param memberId 유저
     * @param boardId 게시판
     * @return a투표
     */
    @PostMapping("/new/a/member-id/{memberId}/board-id/{boardId}")
    @Operation(summary = "게시판 A투표")
    public CommonResult setVoteBoardA(@PathVariable long memberId, @PathVariable long boardId) {
        Member member = memberService.getMemberData(memberId);
        Board board = boardService.getBoardData(boardId);
        voteBoardService.setVoteBoardA(member, board);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param memberId 유저
     * @param boardId 게시판
     * @return b투표
     */
    @PostMapping("/new/b/member-id/{memberId}/board-id/{boardId}")
    @Operation(summary = "게시판 B투표")
    public CommonResult setVoteBoardB(@PathVariable long memberId, @PathVariable long boardId) {
        Member member = memberService.getMemberData(memberId);
        Board board = boardService.getBoardData(boardId);
        voteBoardService.setVoteBoardB(member, board);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @return 투표한 게시글들 전부 확인하기용 리스트겟
     */
    @GetMapping("/all")
    @Operation(summary = "투표한 게시글 전부 불러오기 백 확인용")
    public ListResult<VoteBoardItem> getVoteBoards() {
        return ResponseService.getListResult(voteBoardService.getVoteBoards());
    }

    /**
     *
     * @param memberId 유저가 투표한 게시글만
     * @return 리스트로 가져오기
     */
    @GetMapping("/member-id/{memberId}/all")
    @Operation(summary = "유저가 투표한 게시글만 전부 불러오기")
    public ListResult<VoteBoardItem> getMemberVoteBoards(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(voteBoardService.getMemberVoteBoards(member));
    }

//    /**
//     *
//     * @param voteBoardId 투표
//     * @return 삭제하기
//     */
//    @DeleteMapping("/voteBoard-id/{voteBoardId}/")
//    public CommonResult delVoteBoard(@PathVariable long voteBoardId) {
//        voteBoardService.delVoteBoard(voteBoardId);
//
//        return ResponseService.getCommonResult();
//    }
}
