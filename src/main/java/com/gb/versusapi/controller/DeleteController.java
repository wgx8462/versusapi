package com.gb.versusapi.controller;


import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.service.DeleteService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/delete")
public class DeleteController {
    private final DeleteService deleteService;

    /**
     *
     * @return 연결된 fk 전부 삭제한다.
     */
    @DeleteMapping("/board-fk/all/{boardId}")
    @Operation(summary = "게시글 fk 전부 삭제")
    public CommonResult delBoardAllClear(@PathVariable long boardId) {
        deleteService.delBoardAllClear(boardId);

        return ResponseService.getCommonResult();
    }

    @DeleteMapping("/vote-board-fk/all/{voteBoardId}")
    @Operation(summary = "투표 fk 전부 삭제")
    public CommonResult delVoteBoardAllClear(@PathVariable long voteBoardId) {
        deleteService.delVoteBoardAllClear(voteBoardId);

        return ResponseService.getCommonResult();
    }

    @DeleteMapping("/comment-fk/all/{commentId}")
    @Operation(summary = "댓글 fk 전부 삭제")
    public CommonResult delCommentAllClear(@PathVariable long commentId) {
        deleteService.delCommentAllClear(commentId);

        return ResponseService.getCommonResult();
    }
}
