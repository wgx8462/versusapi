package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Comment;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.recommendComment.RecommendCommentIsLikeItem;
import com.gb.versusapi.model.recommendComment.RecommendCommentItem;
import com.gb.versusapi.service.CommentService;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.RecommendCommentService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/recommend-comment")
public class RecommendCommentController {
    private final MemberService memberService;
    private final CommentService commentService;
    private final RecommendCommentService recommendCommentService;

    /**
     *
     * @param memberId 유저가
     * @param commentId 댓글에 좋아요 표시하기
     * @return 한번 누르면 좋아요 한번더 누르면 취소
     */
    @PostMapping("/new/member-id/{memberId}/comment-id/{commentId}")
    @Operation(summary = "댓글 좋아요 한번누르면 좋아요 한번더 누르면 취소")
    public CommonResult setRecommendComment(@PathVariable long memberId, @PathVariable long commentId) {
        Member member = memberService.getMemberData(memberId);
        Comment comment = commentService.getCommentData(commentId);
        recommendCommentService.setRecommendComment(member, comment);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param memberId 유저가 좋아요한 댓글
     * @return 댓글에 좋아요한 게시글 전부 가져오기
     */
    @GetMapping("/is-like/all/member-id/{memberId}")
    @Operation(summary = "유저가 좋아요한 댓글 전부 가져오기")
    public ListResult<RecommendCommentIsLikeItem> getCommentIsLikes(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(recommendCommentService.getCommentIsLikes(member));
    }

    /**
     *
     * @return 게시글 안에 댓글에 좋아요한 게시글 전부 가져오기 확인용
     */
    @GetMapping("/all")
    @Operation(summary = "댓글 좋아요 전부 가져오기 백 확인용")
    public ListResult<RecommendCommentItem> getRecommendComments() {
        return ResponseService.getListResult(recommendCommentService.getRecommendComments());
    }

    /**
     *
     * @param recommendCommentId 댓글에 한 좋아요
     * @return 삭제하기
     */
    @DeleteMapping("/recommendComment-id/{recommendCommentId}/{commentId}")
    @Operation(summary = "댓글 좋아요 삭제")
    public CommonResult delRecommendComment(@PathVariable long recommendCommentId) {
        recommendCommentService.delRecommendComment(recommendCommentId );

        return ResponseService.getCommonResult();
    }

//    /**
//     *
//     * @param commentId 댓글에 달린 좋아요
//     * @return 전부 삭제
//     */
//    @DeleteMapping("/comment-list/{commentId}/all")
//    public CommonResult delCommentLists(@PathVariable long commentId) {
//        Comment comment = commentService.getCommentData(commentId);
//        recommendCommentService.delCommentLists(comment);
//
//        return ResponseService.getCommonResult();
//    }
}
