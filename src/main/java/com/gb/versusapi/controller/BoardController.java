package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Category;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.model.board.*;
import com.gb.versusapi.service.BoardService;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;



@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final MemberService memberService;
    private final BoardService boardService;

    /**
     *
     * @param memberId 유저
     * @param request 게시글 등록
     * @return
     */
    @PostMapping("/new/member-id/{memberId}")
    @Operation(summary = "게시글 등록")
    public CommonResult setMember(@PathVariable long memberId, @RequestBody @Valid BoardCreateRequest request) {
        Member member = memberService.getMemberData(memberId);
        boardService.setBoard(member, request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @return 게시글 최신순으로 전부 불러오기
     */
    @GetMapping("/all")
    @Operation(summary = "게시글 최신순으로 불러오기")
    public ListResult<BoardItem> getBoards() {
        return ResponseService.getListResult(boardService.getBoards());
    }

    /**
     *
     * @param pageNum 게시글 최신으로 불러오기
     * @return 페이징처리
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "게시글 최신순으로 불러오기 페이징")
    public ListResult<BoardItem> getBoards2(@PathVariable int pageNum) {
        return boardService.getBoards2(pageNum);
    }

    /**
     *
     * @param memberId 유저가 쓴 게시글
     * @return 유저가 쓴 게시글만 리스트로 가져오기
     */
    @GetMapping("/member-id/{memberId}//all")
    @Operation(summary = "유저가 쓴 게시글만 리스트로 가져오기")
    public ListResult<BoardItem> getMemberBoards(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(boardService.getMemberBoards(member));
    }

    @GetMapping("/member-id/{memberId}/{pageNum}/all")
    @Operation(summary = "유저가 쓴 게시글만 리스트로 가져오기 페이징")
    public ListResult<BoardItem> getMemberBoards2(@PathVariable long memberId, @PathVariable int pageNum) {
        Member member = memberService.getMemberData(memberId);
        return boardService.getMemberBoards2(member, pageNum);
    }

    /**
     *
     * @return 게시글 추천수 10개 이상인 탑10개 가져오기
     */
    @GetMapping("/top10/all")
    @Operation(summary = "게시글 추천수10개 이상인 탑10를 가져온다.")
    public ListResult<BoardItem> getLikeBoards() {
        return ResponseService.getListResult(boardService.getLikeBoards());
    }

    /**
     *
     * @param category 카테고리를 선택하면 그 게시글만
     * @return 리스트로 보여준다.
     */
    @GetMapping("/category/all")
    @Operation(summary = "카테고리 게시글만 리스트로 가져오기")
    public ListResult<BoardItem> getCateBoards(@RequestParam(name = "category")Category category) {
        return ResponseService.getListResult(boardService.getCateBoards(category));
    }

//    @GetMapping("/member-id/{memberId}/count")
//    public ListResult<BoardMemberSizeItem> getMemberBoardSize(@PathVariable long memberId) {
//        Member member = memberService.getMemberData(memberId);
//        return ResponseService.getListResult(boardService.getMemberBoardSize(member));
//    }

    /**
     *
     * @param boardId 게시글
     * @return 상세보기
     */
    @GetMapping("/detail/board-id/{boardId}")
    @Operation(summary = "게시글 상세보기")
    public SingleResult<BoardResponse> getBoard(@PathVariable long boardId) {
        return ResponseService.getSingleResult(boardService.getBoard(boardId));
    }

    /**
     *
     * @param topicA 토픽a
     * @return 검색
     */
    @GetMapping("/search/topic")
    @Operation(summary = "토픽a 검색")
    public ListResult<BoardItem> getTopicSearch(@RequestParam(name = "topicA") String topicA) {
        return ResponseService.getListResult(boardService.getTopicSearch(topicA));
    }

    @GetMapping("/topicA-topicB/search/{pageNum}")
    @Operation(summary = "토픽a 토픽b 검색 페이징처리")
    public ListResult<BoardItem> getTopicABSearch(@RequestParam(name = "topicA", required = false) String topicA,@RequestParam(name = "topicB", required = false) String topicB, @PathVariable int pageNum) {
        return boardService.getTopicABSearch(topicA, topicB, pageNum);
    }

    // 게시글 카테고리 변경
    @PutMapping("/change/category/board-id/{boardId}")
    @Operation(summary = "게시글 카테고리 변경")
    public CommonResult putCategory (@PathVariable long boardId, @RequestBody @Valid BoardCategoryChangeRequest request) {
        boardService.putCategory(boardId, request);

        return ResponseService.getCommonResult();
    }

//    /**
//     *
//     * @param boardId 게시글
//     * @return 게시글 삭제 조건 아래 연결된 다른 것들부터 먼저 다 삭제하고 와야한다.
//     */
//    @DeleteMapping("/board-id/{boardId}")
//    public CommonResult delBoard(@PathVariable long boardId) {
//        boardService.delBoard(boardId);
//
//        return ResponseService.getCommonResult();
//    }
}
