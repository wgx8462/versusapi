package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.model.member.*;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    /**
     *
     * @param request 회원가입 정보
     * @return
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    @PostMapping("/join")
    @Operation(summary = "회원가입 아이디중복,비밀번호 일치하지 않을경우 불가")
    public CommonResult setMember(@RequestBody @Valid MemberCreateRequest request) throws Exception {
        memberService.setMember(request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param username 유저아이디 중복확인
     * @return
     */
    @GetMapping("/check/id")
    @Operation(summary = "유저아이디 중복확인")
    public SingleResult<MemberDupCheckResponse> getMemberDupCheck(@RequestParam(name = "username") String username) {
        return ResponseService.getSingleResult(memberService.getMemberIdDupCheck(username));
    }

    /**
     *
     * @return 회원정보 다 불러오기
     */
    @GetMapping("/all")
    @Operation(summary = "회원정보 다 불러오기")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers());
    }

    /**
     *
     * @param pageNum 회원정보 페이징
     * @return 리스트로 불러오기
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "회원정보 다 불러오는데 페이징함")
    public ListResult<Member> getMembers2(@PathVariable int pageNum) {

        return memberService.getMembers2(pageNum);
    }

    /**
     *
     * @param id 선택한 회원정보
     * @return 상세보기
     */
    @GetMapping("/detail/id/{id}")
    @Operation(summary = "회원정보 상세보기")
    public SingleResult<MemberResponse> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }


    /**
     *
     * @param name 유저이름 검색
     * @return 검색완료
     */
    @GetMapping("/name/search")
    @Operation(summary = "유저이름 검색")
    public ListResult<MemberNameSearchRequest> getSearch(@RequestParam(name = "name") String name) {
        return ResponseService.getListResult(memberService.getSearch(name));

    }

    /**
     *
     * @param username 유저아이디 검색
     * @return 검색완료
     */
    @GetMapping("/username/search")
    @Operation(summary = "유저아이디 검색")
    public SingleResult<MemberResponse> getSearchUsername(@RequestParam(name = "username") String username) {
        return ResponseService.getSingleResult(memberService.getSearchUsername(username));
    }

    /**
     *
     * @return 멤버 테이블에 남자 여자 수 가져오기
     */
    @GetMapping("/gender/statistics")
    @Operation(summary = "남자, 여자 회원수")
    public ListResult<MemberGenderStats> getGenders() {
        return ResponseService.getListResult(memberService.getGenders());
    }

    /**
     *
     * @param memberId 선택한 회원
     * @param request 닉네임 변경
     * @return 저장
     */
    @PutMapping("/change/nick-name/member-id/{memberId}")
    @Operation(summary = "닉네임 변경")
    public CommonResult putNickName (@PathVariable long memberId, @RequestBody @Valid MemberNickNameChangeRequest request) {
        memberService.putNickName(memberId, request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param memberId 선택한 회원
     * @param request 비밀번호 변경
     * @return 저장
     * @throws Exception 비밀번호 확인 실패하면 던져
     */
    @PutMapping("/change/password/member-id/{memberId}")
    @Operation(summary = "비밀번호 변경")
    public CommonResult putPassword (@PathVariable long memberId, @RequestBody @Valid MemberPasswordChangeRequest request) throws Exception {
        memberService.putPassword(memberId, request);

        return ResponseService.getCommonResult();
    }


    /**
     *
     * @param id
     * @param request 회원 상태 수정
     * @return
     */
    @PutMapping("/change-rating/{id}")
    @Operation(summary = "회원 상태 수정")
    public CommonResult putMemberRatingChange(@PathVariable long id, @RequestBody @Valid MemberRatingChangeRequest request) {
        memberService.putMemberRatingChange(id, request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param id
     * @param request 회정 등급 수정
     * @return
     */
    @PutMapping("/change-grade/{id}")
    @Operation(summary = "회원 등급 수정")
    public CommonResult putMemberGradeChange(@PathVariable long id, @RequestBody @Valid MemberGradeChangeRequest request) {
        memberService.putMemberGradeChange(id, request);

        return ResponseService.getCommonResult();
    }



}
