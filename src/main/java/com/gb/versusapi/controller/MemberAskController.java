package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.model.memberAsk.MemberAskCreateRequest;
import com.gb.versusapi.model.memberAsk.MemberAskEditRequest;
import com.gb.versusapi.model.memberAsk.MemberAskItem;
import com.gb.versusapi.model.memberAsk.MemberAskResponse;
import com.gb.versusapi.service.MemberAskService;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-ask")



public class MemberAskController {

    private final MemberAskService memberAskService;
    private final MemberService memberService;

    // 문의글 등록
    @PostMapping("/new/{memberId}")
    @Operation(summary = "문의글 등록")
    public CommonResult setMemberAsk(@PathVariable long memberId, @RequestBody @Valid MemberAskCreateRequest request){
        Member member = memberService.getMemberData(memberId);
        memberAskService.setMemberAsk(member,request);

        return ResponseService.getCommonResult();
    }

    // 문의글 전체 리스트 보기
    @GetMapping("/all")
    @Operation(summary = "문의글 전체 리스트로 보기")
    public ListResult<MemberAskItem> getMemberAsks() {
        return ResponseService.getListResult(memberAskService.getMemberAsks());
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의글 전체 리스트로 보기 페이징")
    public ListResult<MemberAskItem> getMemberAsks2(@PathVariable int pageNum) {
        return memberAskService.getMemberAsks2(pageNum);
    }

    // 유저가 쓴 문의글 전체 리스트 보기
    @GetMapping("/{memberId}/all")
    @Operation(summary = "유저가 쓴 문의글 전체 리스트 보기")
    public ListResult<MemberAskItem> getMemberAskList(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(memberAskService.getMemberAskList(member));
    }

    // 문의글 상세보기
    @GetMapping("/detail/memberAsk-id/{memberAskId}")
    @Operation(summary = "문의글 상세보기")
    public SingleResult<MemberAskResponse> getMemberAsk(@PathVariable long memberAskId) {
        return ResponseService.getSingleResult(memberAskService.getMemberAsk(memberAskId));
    }

    // 문의글 수정
    @PutMapping("/edit/memberAsk-id/{memberAskId}")
    @Operation(summary = "문의글 수정")
    public CommonResult putAskEdit(@PathVariable long memberAskId, @RequestBody @Valid MemberAskEditRequest request){
        memberAskService.putAskEdit(memberAskId,request);

        return ResponseService.getCommonResult();
    }

    // 문의글 삭제
    @DeleteMapping("/delete/{id}")
    @Operation(summary = "문의글 삭제")
    public CommonResult delAsk(@PathVariable long id){
        memberAskService.delAsk(id);

        return ResponseService.getCommonResult();
    }


}

