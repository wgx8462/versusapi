package com.gb.versusapi.controller;

import com.gb.versusapi.entity.MemberAsk;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.model.askManagement.AskManagementCreateRequest;
import com.gb.versusapi.model.askManagement.AskManagementItem;
import com.gb.versusapi.model.askManagement.AskManagementResponse;
import com.gb.versusapi.service.AskManagementService;
import com.gb.versusapi.service.MemberAskService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ask-management")
public class AskManagementController {
    private final MemberAskService memberAskService;
    private final AskManagementService askManagementService;

    // 문의글 답변 작성하
    @PostMapping("/join/member-ask-id/{memberAskId}")
    @Operation(summary = "문의글 답변 등록")
    public CommonResult setAskManagement(@PathVariable long memberAskId, @RequestBody @Valid AskManagementCreateRequest request) {
        MemberAsk memberAsk = memberAskService.getMemberAskData(memberAskId);
        askManagementService.setAskManagement(memberAsk, request);

        return ResponseService.getCommonResult();
    }

    // 답변한 문의글 전부 가져오기
    @GetMapping("/all")
    @Operation(summary = "답변한 문의글 전부 가져오기")
    public ListResult<AskManagementItem> getAskManagements() {
        return ResponseService.getListResult(askManagementService.getAskManagements());
    }

    @GetMapping("/{pageNum}/all")
    @Operation(summary = "답변한 문의글 전부 가져오기 페이징")
    public ListResult<AskManagementItem> getAskManagements2(@PathVariable int pageNum) {
        return askManagementService.getAskManagements2(pageNum);
    }

    // 답변한 문의글 상세보기
    @GetMapping("/detail/ask-management-id/{askManagementId}")
    @Operation(summary = "답변한 문의글 상세보기")
    public SingleResult<AskManagementResponse> getAskManagement(@PathVariable long askManagementId) {
        return ResponseService.getSingleResult(askManagementService.getAskManagement(askManagementId));
    }

    // 문의글 답변한거 삭제 이거부터 삭제해야 문의글을 삭제할수있음
    @DeleteMapping("/delete/ask-management-id/{askManagementId}")
    @Operation(summary = "문의글 답변 삭제하기")
    public CommonResult delAskManagement(@PathVariable long askManagementId) {
        askManagementService.delAskManagement(askManagementId);

        return ResponseService.getCommonResult();
    }
}
