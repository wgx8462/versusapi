package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.model.comment.*;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class CommentController {
    private final MemberService memberService;
    private final BoardService boardService;
    private final VoteBoardService voteBoardService;
    private final CommentService commentService;

    /**
     *
     * @param memberId 유저
     * @param voteBoardId 투표
     * @param request 투표한거에 댓글 쓰기 자기가 투푠한게 아니면 등록이 안된다.
     * @return
     */
    @PostMapping("/new/member-id/{memberId}/voteBoard-id/{voteBoardId}")
    @Operation(summary = "투표한 사람만 댓글 등록 가능")
    public CommonResult setComment(@PathVariable long memberId, @PathVariable long voteBoardId, @RequestBody @Valid CommentCreateRequest request) {
        Member member = memberService.getMemberData(memberId);
        VoteBoard voteBoard = voteBoardService.getVoteBoardData(voteBoardId);
        commentService.setComment(member, voteBoard, request);

        return ResponseService.getCommonResult();
    }

    // 댓글 작성한 게시글 상세보기
    @GetMapping("/detail/comment-id//{commentId}")
    @Operation(summary = "댓글 작성한 게시글 상세보기?")
    public SingleResult<CommentResponse> getCommentResponse (@PathVariable long commentId) {
        return ResponseService.getSingleResult(commentService.getCommentResponse(commentId));
    }

    /**
     *
     * @return 댓글 달린 게시글 전부 불러오기
     */
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "댓글 달린 게시글 전부 불러오기 페이징")
    public ListResult<CommentItemPage> getComments(@PathVariable int pageNum) {
        return commentService.getComments(pageNum);
    }

    /**
     *
     * @param memberId 유저가 쓴 댓글
     * @return 리스트로 불러오기
     */
    @GetMapping("/member-id/{memberId}")
    @Operation(summary = "유저가 쓴 댓글 리스트로 가져오기")
    public ListResult<CommentItems> getMemberComment(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(commentService.getMemberComment(member));
    }

    /**
     *
     * @param boardId 게시글에 등록된 댓글
     * @param pageNum 페이징 처리후
     * @return 리스트로 불러오기
     */
    @GetMapping("/board/vote-board/{boardId}/{pageNum}")
    @Operation(summary = "게시글에 쓴 댓글 등록순으로 불러오기 페이징")
    public ListResult<CommentVoteBoardItem> getBoardVoteComments(@PathVariable long boardId, @PathVariable int pageNum) {
        Board board = boardService.getBoardData(boardId);
        return commentService.getBoardVoteComments(board, pageNum);
    }

    /**
     *
     * @param memberId 유저가 댓글단 게시글
     * @return 유저가 댓글단 게시글을 리스트로 전부 가져오기
     */
    @GetMapping("/member-id/board/{memberId}/all")
    @Operation(summary = "댓글 달린 게시글들 전부 리스트로 가져오기")
    public ListResult<CommentItem> getMemberComments(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(commentService.getMemberComments(member));
    }

//    /**
//     *
//     * @param commentId 댓글
//     * @return 댓글 삭제하기
//     */
//    @DeleteMapping("/comment-id/{commentId}")
//    public CommonResult delComment(@PathVariable long commentId) {
//        commentService.delComment(commentId);
//
//        return ResponseService.getCommonResult();
//    }
}
