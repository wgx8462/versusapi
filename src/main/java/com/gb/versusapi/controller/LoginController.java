package com.gb.versusapi.controller;

import com.gb.versusapi.model.login.LoginRequest;
import com.gb.versusapi.model.login.LoginResponse;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/member")
    @Operation(summary = "로그인")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest request) {
        return loginService.doLogin(request);
    }
}
