package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.recommendBoard.RecommendBoardIsLikeItem;
import com.gb.versusapi.model.recommendBoard.RecommendBoardItem;
import com.gb.versusapi.service.BoardService;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.RecommendBoardService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/recommend-board")
public class RecommendBoardController {
    private final MemberService memberService;
    private final BoardService boardService;
    private final RecommendBoardService recommendBoardService;

    /**
     *
     * @param memberId 유저가
     * @param boardId 게시글 좋아요
     * @return 한번 누르면 좋아요 한번더 누르면 취소
     */
    @PostMapping("/new/member-id/{memberId}/board-id/{boardId}/")
    @Operation(summary = "게시글 한번 누르면 좋아요 한번더 누르면 취소")
    public CommonResult setRecommendBoard(@PathVariable long memberId, @PathVariable long boardId) {
        Member member = memberService.getMemberData(memberId);
        Board board = boardService.getBoardData(boardId);
        recommendBoardService.setRecommendBoard(member, board);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param memberId 유저가
     * @return 좋아요 한 게시글 전부 불러오기
     */
    @GetMapping("/is-like/all/member-id/{memberId}")
    @Operation(summary = "좋아요 한 게시글 전부 불러오기")
    public ListResult<RecommendBoardIsLikeItem> getRecommendBoardIsLikes(@PathVariable long memberId) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(recommendBoardService.getRecommendBoardIsLikes(member));
    }

    /**
     *
     * @return 좋아요 있는 게시글 전부 가져오기
     */
    @GetMapping("/all")
    @Operation(summary = "좋아요 있는 게시글 전부 불러오기")
    public ListResult<RecommendBoardItem> getRecommendBoards() {
        return ResponseService.getListResult(recommendBoardService.getRecommendBoards());
    }

    /**
     *
     * @param recommendBoardId 좋아요
     * @return 삭제
     */
    @DeleteMapping("/recommendBoard-id/{recommendBoardId}/")
    @Operation(summary = "좋아요 삭제")
    public CommonResult delRecommendBoard(@PathVariable long recommendBoardId) {
        recommendBoardService.delRecommendBoard(recommendBoardId);

        return ResponseService.getCommonResult();
    }

//    /**
//     *
//     * @param boardId 게시글 좋아요 전부
//     * @return 삭제하기
//     */
//    @DeleteMapping("/board-list/{boardId}")
//    public CommonResult delboardList(@PathVariable long boardId) {
//        Board board = boardService.getBoardData(boardId);
//        recommendBoardService.delBoardList(board);
//
//        return ResponseService.getCommonResult();
//    }
}
