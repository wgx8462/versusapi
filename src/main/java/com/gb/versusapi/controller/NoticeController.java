package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.model.notice.NoticeChangeRequest;
import com.gb.versusapi.model.notice.NoticeCreateRequest;
import com.gb.versusapi.model.notice.NoticeItem;
import com.gb.versusapi.model.notice.NoticeResponse;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.NoticeService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/notice")
public class NoticeController {
    private final MemberService memberService;
    private final NoticeService noticeService;

    /**
     *
     * @param memberId
     * @param request 공지글 등록
     * @return
     */
    @PostMapping("/new/member-id/{memberId}")
    @Operation(summary = "공지글 등록")
    public CommonResult setNotice(@PathVariable long memberId, @RequestBody @Valid NoticeCreateRequest request) {
        Member member = memberService.getMemberData(memberId);
        noticeService.setNotice(member, request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @return 공지글 전부 불러오기
     */
    @GetMapping("/all")
    @Operation(summary = "공지글 전부 불러오기")
    public ListResult<NoticeItem> getNotices() {

        return ResponseService.getListResult(noticeService.getNotices());
    }

    /**
     *
     * @param noticeId 공지글
     * @return 상세보기
     */
    @GetMapping("/detail/notice-id/{noticeId}")
    @Operation(summary = "공지글 상세보기")
    public SingleResult<NoticeResponse> getNotice(@PathVariable long noticeId) {

        return ResponseService.getSingleResult(noticeService.getNotice(noticeId));
    }

    /**
     *
     * @param noticeId 공지글
     * @param request 수정하기
     * @return
     */
    @PutMapping("/change-notice/notice-id/{noticeId}")
    @Operation(summary = "공지글 수정하기")
    public CommonResult putNotice(@PathVariable long noticeId, @RequestBody @Valid NoticeChangeRequest request) {
        noticeService.putNotice(noticeId, request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @param noticeId 공지글
     * @return 삭제하기
     */
    @DeleteMapping("/notice-id/{noticeId}")
    @Operation(summary = "공지글 삭제하기")
    public CommonResult delNotice(@PathVariable long noticeId) {
        noticeService.delNotice(noticeId);

        return ResponseService.getCommonResult();
    }

}
