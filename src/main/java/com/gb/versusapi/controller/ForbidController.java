package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Forbid;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.ListResult;
import com.gb.versusapi.model.forbid.ForbidCreateRequest;
import com.gb.versusapi.model.forbid.ForbidItem;
import com.gb.versusapi.service.ForbidService;
import com.gb.versusapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/forbid")
public class ForbidController {
    private final ForbidService forbidService;

    /**
     *
     * @param request 금칙어를 등록하는데
     * @return 중복이면 등록이 안된다.
     */
    @PostMapping("/new")
    @Operation(summary = "금칙어 등록 중복이면 안된다.")
    public CommonResult setForbid (@RequestBody @Valid ForbidCreateRequest request) {
        forbidService.setForbid(request);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @return 금칙어를 리스트로 조회해서 불러온다.
     */
    @GetMapping("/all")
    @Operation(summary = "금칙어 리스트로 보기")
    public ListResult<ForbidItem> getForbids () {
        return ResponseService.getListResult(forbidService.getForbids());
    }


    @GetMapping("/{pageNum}/all")
    @Operation(summary = "금칙어 리스트 보기 페이징")
    public ListResult<Forbid> getForbids2(@PathVariable int pageNum) {
        return forbidService.getForbids2(pageNum);
    }
    /**
     *
     * @param forbidId 선택한 금칙어
     * @return 삭제한다.
     */
    @DeleteMapping("/delete/forbid-id/{forbidId}")
    @Operation(summary = "금칙어 삭제")
    public CommonResult delForbid (@PathVariable long forbidId) {
        forbidService.delForbid(forbidId);

        return ResponseService.getCommonResult();
    }

}

