package com.gb.versusapi.controller;

import com.gb.versusapi.model.temp.NearDayStatisticsResponse;
import com.gb.versusapi.model.common.CommonResult;
import com.gb.versusapi.model.common.SingleResult;
import com.gb.versusapi.service.ResponseService;
import com.gb.versusapi.service.TempService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;

    /**
     *
     * @param csvFile 유저 정보 파일
     * @return 데이터 베이스 업로드
     * @throws IOException -
     */
    @PostMapping("/file-upload")
    @Operation(summary = "데이터 베이스 유저 정보 파일 업로드")
    public CommonResult setMemberByFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
        tempService.setMemberByFile(csvFile);

        return ResponseService.getCommonResult();
    }

    /**
     *
     * @return 최근 7일간의 회원 가입자 수를 구한다.
     */
    @GetMapping("/member-join-day-list")
    @Operation(summary = "최근 7일간의 회원 가입자 수를 구한다.")
    public SingleResult<NearDayStatisticsResponse> getNearDayStatistics() {
        return ResponseService.getSingleResult(tempService.getNearDayStatistics());
    }
}
