package com.gb.versusapi.model.forbid;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Getter
@Setter
public class ForbidCreateRequest {
    @NotNull
    @Length(min = 1,max = 20)
    private String forbidName;
}

