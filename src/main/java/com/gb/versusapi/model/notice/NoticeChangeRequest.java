package com.gb.versusapi.model.notice;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class NoticeChangeRequest {

    @Length(min = 2, max = 20)
    private String title;

    private String content;
}
