package com.gb.versusapi.model.notice;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class NoticeItem {
    private Long id;
    private String memberName;
    private String title;
    private LocalDateTime dateNotice;
}
