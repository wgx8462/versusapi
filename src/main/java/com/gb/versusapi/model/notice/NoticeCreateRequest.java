package com.gb.versusapi.model.notice;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class NoticeCreateRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String title;

    @NotNull
    private String content;
}
