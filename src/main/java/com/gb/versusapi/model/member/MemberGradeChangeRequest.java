package com.gb.versusapi.model.member;

import com.gb.versusapi.enums.Grade;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberGradeChangeRequest {
    @NotNull
    private Grade grade;
}
