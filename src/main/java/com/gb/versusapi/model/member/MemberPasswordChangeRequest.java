package com.gb.versusapi.model.member;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class MemberPasswordChangeRequest {
    @NotNull
    @Length(min = 4, max = 20)
    private String password;

    @NotNull
    @Length(min = 4, max = 20)
    private String passwordRe;
}
