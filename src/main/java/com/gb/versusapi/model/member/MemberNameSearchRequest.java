package com.gb.versusapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberNameSearchRequest {
    private String username;
    private String nickName;
    private String name;
    private String gender;
}
