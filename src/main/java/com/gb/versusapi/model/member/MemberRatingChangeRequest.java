package com.gb.versusapi.model.member;

import com.gb.versusapi.enums.MemberRating;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRatingChangeRequest {
    @NotNull
    private MemberRating memberRating;
}
