package com.gb.versusapi.model.member;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class MemberNickNameChangeRequest {
    @NotNull
    @Length(min = 1, max = 10)
    private String nickName;
}

