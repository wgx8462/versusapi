package com.gb.versusapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String memberRatingName;
    private String gradeName;
    private String username;
    private String nickName;
    private String name;
}
