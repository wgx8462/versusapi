package com.gb.versusapi.model.member;

import com.gb.versusapi.enums.Gender;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;


@Getter
@Setter
public class MemberCreateRequest {
    @NotNull
    @Length(min = 4, max = 15)
    private String username;

    @NotNull
    @Length(min = 4, max = 20)
    private String password;

    @NotNull
    @Length(min = 4, max = 20)
    private String passwordRe;

    @NotNull
    @Length(min = 1, max = 10)
    private String nickName;

    @NotNull
    @Length(min = 2,max = 20)
    private String name;

    @NotNull
    private LocalDate dateBirth;

    @NotNull
    private String phoneNumber;

    @NotNull
    private Gender gender;

    @NotNull
    private String email;

    @NotNull
    private LocalDate dateUser;
}
