package com.gb.versusapi.model.member;

import com.gb.versusapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String memberRatingName;
    private String gradeName;
    private String username;
    private String imgUrl;
    private String nickName;
    private String name;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String gender;
    private String email;
}
