package com.gb.versusapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberGenderStats {
    private Integer manStats;
    private Integer womanStats;
}
