package com.gb.versusapi.model.login;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse {
    private Long memberId;
    private String memberName;
}
