package com.gb.versusapi.model.voteBoard;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class VoteBoardItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private Long boardId;
    private String boardTopicA;
    private String boardTopicB;
    private String isAName;
    private LocalDateTime dateVote;
}
