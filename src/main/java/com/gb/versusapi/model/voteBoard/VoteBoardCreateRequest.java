package com.gb.versusapi.model.voteBoard;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class VoteBoardCreateRequest {
    private Boolean isA;
    private LocalDateTime dateVote;
}
