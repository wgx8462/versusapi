package com.gb.versusapi.model.recommendComment;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecommendCommentItem {
    private Long id;
    private String memberName;
    private Long commentId;
    private String isLike;
}
