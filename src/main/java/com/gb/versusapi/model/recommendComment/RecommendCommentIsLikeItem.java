package com.gb.versusapi.model.recommendComment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RecommendCommentIsLikeItem {
    private Long id;
    private String memberNickName;
    private Long boardId;
    private String boardTopicA;
    private String boardTopicB;
    private LocalDateTime boardDateBoard;
}
