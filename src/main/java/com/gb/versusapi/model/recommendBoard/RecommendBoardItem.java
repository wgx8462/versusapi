package com.gb.versusapi.model.recommendBoard;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecommendBoardItem {
    private Long id;
    private String memberName;
    private Long boardId;
    private String isLike;
}
