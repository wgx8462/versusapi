package com.gb.versusapi.model.recommendBoard;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RecommendBoardIsLikeItem {
    private Long id;
    private String memberNickName;
    private LocalDateTime dateRecommendBoard;
    private String boardMemberNickName;
    private Long boardId;
    private String boardTopicA;
    private String boardTopicB;
    private LocalDateTime boardDateBoard;
}
