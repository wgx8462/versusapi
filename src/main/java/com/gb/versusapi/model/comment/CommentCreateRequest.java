package com.gb.versusapi.model.comment;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentCreateRequest {
    @NotNull
    @Length(min = 2, max = 50)
    private String contents;
}
