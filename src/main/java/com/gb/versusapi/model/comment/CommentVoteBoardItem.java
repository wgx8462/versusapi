package com.gb.versusapi.model.comment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentVoteBoardItem {
    private Long id;
    private String memberNickname;
    private Boolean voteBoardIsA;
    private Long recommendNum;
    private LocalDateTime dateCommentRegister;
}
