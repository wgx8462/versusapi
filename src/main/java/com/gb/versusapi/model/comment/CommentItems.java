package com.gb.versusapi.model.comment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentItems {
    private String contents;
    private Long recommendNum;
    private LocalDateTime dateCommentRegister;
}
