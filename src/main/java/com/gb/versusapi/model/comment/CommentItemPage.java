package com.gb.versusapi.model.comment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter

public class CommentItemPage {
    private Long id;
    private Long memberId;
    private String memberName;
    private String boardTopicA;
    private String boardTopicB;
    private Long voteBoardId;
    private String contents;
    private Long recommendNum;
    private LocalDateTime dateCommentRegister;
}
