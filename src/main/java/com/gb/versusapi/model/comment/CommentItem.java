package com.gb.versusapi.model.comment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private Long voteBoardId;
    private String contents;
    private Long recommendNum;
    private LocalDateTime dateCommentRegister;
}
