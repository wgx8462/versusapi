package com.gb.versusapi.model.memberAsk;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter

public class MemberAskCreateRequest {
    @NotNull
    @Length(min = 2, max = 10)
    private String title;

    @NotNull
    private String content;
}

