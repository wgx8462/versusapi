package com.gb.versusapi.model.memberAsk;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Setter
@Getter

public class MemberAskEditRequest {
    @Length(min = 2, max = 10)
    private String title;

    private String content;

}
