package com.gb.versusapi.model.memberAsk;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MemberAskItem {
    private Long id;
    private String memberNickName;
    private String title;
    private LocalDateTime dateUserAsk;
}
