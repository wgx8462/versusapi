package com.gb.versusapi.model.askManagement;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class AskManagementItem {
    private Long id;
    private String memberAskTitle;
    private LocalDateTime dateAskManagement;
    private Boolean isCompletion;
}
