package com.gb.versusapi.model.askManagement;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AskManagementCreateRequest {
    @NotNull
    private String content;
}
