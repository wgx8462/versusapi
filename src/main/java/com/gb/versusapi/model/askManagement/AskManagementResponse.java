package com.gb.versusapi.model.askManagement;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class AskManagementResponse {
    private Long id;
    private String memberAskTitle;
    private String memberAskContent;
    private String content;
    private LocalDateTime dateAskManagement;
    private Boolean isCompletion;
}
