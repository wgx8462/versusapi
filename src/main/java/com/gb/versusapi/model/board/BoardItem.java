package com.gb.versusapi.model.board;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BoardItem {
    private Long id;
    private Long memberId;
    private String memberImgUrl;
    private String memberNickName;
    private String topicA;
    private String topicB;
    private LocalDateTime dateBoard;
    private Long viewCount;
    private Integer likeCount;

}
