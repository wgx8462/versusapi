package com.gb.versusapi.model.board;

import com.gb.versusapi.enums.Category;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardCategoryChangeRequest {
    @NotNull
    private Category category;
}
