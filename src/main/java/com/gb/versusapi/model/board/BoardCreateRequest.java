package com.gb.versusapi.model.board;

import com.gb.versusapi.enums.Category;
import com.gb.versusapi.enums.PostType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Getter
@Setter
public class BoardCreateRequest {
    @NotNull
    private Category category;

    @NotNull
    private PostType postType;

    @NotNull
    @Length(min = 2, max = 20)
    private String topicA;

    private String contentA;

    @NotNull
    @Length(min = 2, max = 20)
    private String topicB;

    private String contentB;

    private LocalDateTime dateBoard;
    private Boolean isOverTime;
    private Boolean isSecret;
}
