package com.gb.versusapi.model.board;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardMemberSizeItem {
    private Integer booksSize;
}
