package com.gb.versusapi.model.board;

import com.gb.versusapi.enums.Category;
import com.gb.versusapi.enums.PostType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BoardResponse {
    private Long id;
    private String memberImgUrl;
    private String memberNickName;
    private String categoryName;
    private String postTypeName;
    private String topicA;
    private String contentA;
    private Long contentANum;
    private String topicB;
    private String contentB;
    private Long contentBNum;
    private LocalDateTime dateBoard;
    private Long viewCount;
    private Integer likeCount;
    private String isOverTimeName;
}
