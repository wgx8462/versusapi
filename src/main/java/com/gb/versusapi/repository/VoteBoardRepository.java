package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.VoteBoard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VoteBoardRepository extends JpaRepository<VoteBoard, Long> {

    /**
     *
     * @param member 유저가 투표한 게시글
     * @return 유저한 투표한 게시글만 리스트로 가져오기
     */
    List<VoteBoard> findAllByMemberOrderByIdDesc(Member member);

    /**
     *
     * @param member 유저가 있는지 없는지
     * @param board 게시글이 있는지 없는지
     * @return 있으면 있다 없으면 없다 확인한다.
     */
    Optional<VoteBoard> findByMemberAndBoard(Member member, Board board);

    List<VoteBoard> findByBoard(Board board);
    Optional<VoteBoard> findByBoardOrderById(Board board);

    Optional<VoteBoard> findVoteBoardByBoard_Member_Id(long id);

}
