package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Comment;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendComment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RecommendCommentRepository extends JpaRepository<RecommendComment, Long> {

    /**
     *
     * @param member 유저가 추천한 댓글에 게시글을
     * @return 리스트 최신순으로 가져오기
     */
    List<RecommendComment>  findAllByMemberOrderByComment_IdDesc(Member member);

    List<RecommendComment> findByComment(Comment comment);
    List<RecommendComment> findAllByComment(Comment comment);


    /**
     *
     * @param member 유저가 있는지 없는지
     * @param comment 댓글이 있는지 없는지
     * @return 있으면 있다고 없으면 없다고 확인해준다.
     */
    Optional<RecommendComment> findByMemberAndComment(Member member, Comment comment);

}
