package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Forbid;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ForbidRepository extends JpaRepository<Forbid, Long> {
    Optional<Forbid> findByForbidName(String forbidName);
}
