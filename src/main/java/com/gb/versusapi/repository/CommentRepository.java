package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Comment;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.VoteBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface CommentRepository extends JpaRepository<Comment, Long> {

    /**
     *
     * @param member 유저가 댓글단 게시글
     * @return 리스트로 가져오기
     */
    List<Comment> findAllByMemberOrderByIdDesc(Member member);

    List<Comment> findByVoteBoard(VoteBoard voteBoard);

    Optional<Comment> findByMemberAndVoteBoardOrderById(Member member,VoteBoard voteBoard);

    Page<Comment> findAllByVoteBoard_BoardOrderByIdAsc(Board board, Pageable pageable);

}
