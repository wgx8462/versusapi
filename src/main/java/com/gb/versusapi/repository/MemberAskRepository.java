package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.MemberAsk;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberAskRepository extends JpaRepository<MemberAsk, Long> {
    List<MemberAsk> findAllByMemberOrderByIdDesc(Member member);
}
