package com.gb.versusapi.repository;

import com.gb.versusapi.entity.AskManagement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AskManagementRepository extends JpaRepository<AskManagement, Long> {
    Page<AskManagement> findAllByOrderByIdDesc(Pageable Pageable);
}
