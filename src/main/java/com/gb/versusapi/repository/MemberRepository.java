package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Gender;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    /**
     *
     * @param username 유저네임
     * @return 중복검사
     */
    long countByUsername(String username);

    long countByDateUser(LocalDate dateCrate);

    /**
     *
     * @param name 이름
     * @return 검색
     */
    List<Member> findAllByNameContaining(String name);

    Member findByUsernameLike(String username);

    /**
     *
     * @param gender 성별 이넘
     * @return 가져오기 기능
     */
    List<Member> findAllByGender(Gender gender);

    Optional<Member> findByUsernameAndPassword(String username, String password);

}
