package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BoardRepository extends JpaRepository<Board, Long> {

    /**
     *
     * @param id 게시글 아이디 Asc는 기본값이라 써도 안써도 상관이 없다.
     * @return 1번부터 순서대로 가져오기
     */
    List<Board> findAllByIdGreaterThanEqualOrderByIdAsc(long id);
    /**
     *
     * @param id 게시글 아이디
     * @return 역순으로 가져오기(가장 나중에 등록된 게시글부터 가져온다 고로 최신순)
     */
    List<Board> findAllByIdGreaterThanEqualOrderByIdDesc(long id);
    Page<Board> findAllByOrderByIdDesc(PageRequest pageRequest);

    List<Board> findTop10ByLikeCountGreaterThanEqualOrderByLikeCountDesc(long likeCount);
    List<Board> findAllByLikeCountGreaterThanEqualOrderByLikeCount(long likeCount);
    List<Board> findAllByOrderByLikeCountDesc();

    /**
     *
     * @param topic 검색할 단어적는
     * @return 제목에서 검색
     */
    List<Board> findAllByTopicAContaining(String topic);
    List<Board> findAllByTopicBContaining(String topic);
    Page<Board> findAllByTopicAContainingOrTopicBContaining(String topicA, String topicB, Pageable pageable);

    /**
     *
     * @param member 유저가 쓴 게시글
     * @return 리스트로 가져오기 페이징 처리
     */
    List<Board> findAllByMemberOrderByIdDesc(Member member);

    Page<Board> findAllByMemberAndIdGreaterThanEqualOrderByIdDesc(Member member,Pageable pageable, long id);

    /**
     *
     * @param category 카테고리별 게시글
     * @return 리스트로 가져오기
     */
    List<Board> findAllByCategoryOrderByIdDesc(Category category);

}
