package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendBoard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RecommendBoardRepository extends JpaRepository<RecommendBoard, Long> {

    /**
     *
     * @param member 유저 아이디가 추천한 게시글
     * @return 리스트로 가져오기
     */
    List<RecommendBoard> findAllByMemberOrderByBoard_IdDesc(Member member);

    /**
     *
     * @param member 멤버가 있는지 없는지
     * @param board 보드가 있는지 없는지
     * @return 있으면 있다고 알려주고 없으면 없다고 알려준다.
     */
    Optional<RecommendBoard> findByMemberAndBoard(Member member, Board board);

    /**
     *
     * @param board 게시글에 걸려있는 추천 전부 불러오기
     * @return 리스트로 가져온다.
     */
    List<RecommendBoard> findAllByBoard(Board board);

    List<RecommendBoard> findByBoard(Board board);
}
