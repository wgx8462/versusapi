#  VERSUS 프로젝트
### 팀명: 봄봄봄 (3인)
### 팀원 소개 
<img src="images/kb.png" width= 150 alt="내사진"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<img src="images/yj.png" width= 150 alt="내사진"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/me02.jpg" width= 150 alt="내사진"/>


**백엔드 : 김기범** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**프론트 : 이윤진** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**프론트 : 홍수경**

### 프로젝트 소개

* 

### 개발기간 : 2023 - 12.20 ~ 2024 - 02.29
---

### 사용한 기술
* **Spring boot, Docker, PostgreSQL, Google Cloud Platform(GCP)**
### 프로젝트 아키텍처
![사용한 기술](./images/image.png)
### 프로젝트 주요 기능

* 

### 프론트 깃 주소 
#### **웹 :** [이동하기](https://gitlab.com/versus-project/vs-admin)

#### **앱 :** [이동하기](https://gitlab.com/lee_rado/versus_app)